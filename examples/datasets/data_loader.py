from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os.path
from os.path import join

import pandas as pd

from .synthetic import make_synthetic_dataset
from .uci_adult import make_uci_adult_dataset


def load(experiment_name, data_folder, data_file):
    path_to_file = join(data_folder, data_file)
    if experiment_name == 'synthetic':
        if not os.path.exists(path_to_file):
            make_synthetic_dataset(data_folder, data_file)
        return pd.read_csv(path_to_file)
    elif experiment_name == 'uci_adult':
        if not os.path.exists(path_to_file):
            make_uci_adult_dataset(data_folder, data_file)
        return pd.read_csv(path_to_file)

    else:
        raise ValueError('No experiment named {:s}.'.format(experiment_name))
