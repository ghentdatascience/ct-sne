from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from os.path import join

import pandas as pd
import numpy as np
from scipy import stats


def sample_point(centroids, d):
    c_num, c_dim = centroids.shape
    point = []

    # randomly select a cluster in dim 1-4
    i = np.random.randint(c_num)

    ratio = 3
    # randomly select a cluster in dime 5-6
    mean = np.random.choice([-1, 1], 2)/ratio

    # append labels
    point.append(i)
    j = int((mean[0]*ratio + 1) + (mean[1]*ratio + 1) / 2)
    point.append(j)

    # sample dim 1-4
    point.extend(centroids[i, ] + np.random.randn(c_dim) * 0.1)

    # sample dim 5-6
    point.extend(np.random.randn(2) * 0.1 + mean)

    # sample dim 7-10
    point.extend(np.random.randn(d - c_dim - 2))
    return np.array(point)


def make_synthetic_dataset(data_folder, data_file, n_samples=1000, n_dim = 10):
    n_cluster_1_4 = 5
    centroids = np.random.randn(n_cluster_1_4, 4) * 5
    data = [sample_point(centroids, n_dim) for i in range(n_samples)]

    col_names = ['cluster_1_4', 'cluster_5_6']
    for i in range(n_dim):
        col_names.append('d_' + str(i))
    df_result = pd.DataFrame(data, columns=col_names)
    start_col = df_result.columns.get_loc('d_0')
    df_result.iloc[:, start_col:] = \
        stats.zscore(df_result.iloc[:, start_col:].values)

    # label combinations
    labels, _ = pd.factorize(df_result['cluster_1_4'].astype(str) +
                             df_result['cluster_5_6'].astype(str))
    df_result.insert(2, 'cluster_1_6', labels)

    # name the id column
    df_result.index.name = 'id'

    # change label type to int
    df_result['cluster_1_4'] = df_result['cluster_1_4'].astype(np.int32)
    df_result['cluster_5_6'] = df_result['cluster_5_6'].astype(np.int32)
    df_result['cluster_1_6'] = df_result['cluster_1_6'].astype(np.int32)

    # output as csv
    df_result.to_csv(join(data_folder, data_file))
