from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
import pickle
import random
from os.path import join

import numpy as np
import pandas as pd
import pylab as plt

import datasets.data_loader as data_loader
from utils import compute_embedding

WORK_FOLDER = './'
CACHE_FOLDER = 'embeddings'
FIGURE_FOLDER = 'figures'
if not os.path.exists(join(WORK_FOLDER, CACHE_FOLDER)):
    os.makedirs(join(WORK_FOLDER, CACHE_FOLDER))

if not os.path.exists(join(WORK_FOLDER, FIGURE_FOLDER)):
    os.makedirs(join(WORK_FOLDER, FIGURE_FOLDER))

seed=0
random.seed(seed)
np.random.seed(seed)

case_study_name = 'synthetic'
data_folder = './datasets/synthetic'
data_file = 'synthetic_data.csv'
figure_folder = './figures'
emb_folder = './embeddings/'

df_data = data_loader.load(case_study_name, data_folder, data_file)

'''
Compute embeddings with different prior
'''
emb_names = ['tsne', 'ctsne_prior_1_4', 'ctsne_prior_1_6']
methods = ['tsne', 'ctsne', 'ctsne']
prior_cols = [None, 'cluster_1_4', 'cluster_1_6']
embs = {}
for emb_name, prior_col, method in zip(emb_names, prior_cols, methods):
    emb_file = '{:s}_{:s}'.format(case_study_name, method)
    emb_file += '_{:s}'.format(prior_col) if prior_col else '.csv'
    emb_path = join(emb_folder, emb_file)
    embs[emb_name] = compute_embedding(df_data, emb_path, method=method,
                                       prior_col=prior_col)

color_col = 'cluster_1_4'
color_ids = pd.factorize(df_data[color_col])[0]
colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
          '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
          '#bcbd22', '#17becf']
marker_col = 'cluster_5_6'
marker_ids = pd.factorize(df_data[marker_col])[0]
marker_size = 10
titles = ['(a)', '(b)', '(c)', '(d)']

def plot_embedding(ax, Y, marker_size, color_ids, marker_ids, title,
                   markers=None, facecolors=None, edgecolors=None,
                   linewidth=1.0, fontsize=12, alpha=0.5):
    if markers == None:
        markers = ['o', 'o', 'o', 'o']
    for marker_id in np.unique(marker_ids):
        mask = marker_ids == marker_id
        sub_colors = [colors[k] for k in color_ids[mask]]
        if facecolors != 'none':
            facecolors = sub_colors
        if edgecolors != 'none':
            edgecolors = sub_colors
        ax.scatter(Y[mask, 0], Y[mask, 1], marker_size, edgecolors=edgecolors,
                   marker=markers[marker_id], alpha=alpha,
                   facecolors=facecolors, linewidth=linewidth)
        ax.annotate(title, xy=(0, 0.96), xycoords='axes fraction',
                    fontsize=fontsize)
        ax.set_axis_off()

figure_file = 'synthetic.pdf'
fig, ax = plt.subplots(1, 3, figsize=(10, 2.5))
for i, emb_name in enumerate(emb_names):
    plot_embedding(ax[i], embs[emb_name], marker_size, color_ids,
                   marker_ids, titles[i], edgecolors='none', alpha=0.7)
plt.subplots_adjust(left=0.0, bottom=0.0, right=1, top=1,
                    wspace=0.05, hspace=None)
fig.savefig(join(figure_folder, figure_file))
fig.tight_layout()
plt.close()
