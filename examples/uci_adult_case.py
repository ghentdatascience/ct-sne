from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
from os.path import join

import random
import numpy as np
import pandas as pd
import pylab as plt
from matplotlib.markers import MarkerStyle


import datasets.data_loader as data_loader
from utils import compute_embedding

WORK_FOLDER = './'
CACHE_FOLDER = 'embeddings'
FIGURE_FOLDER = 'figures'
if not os.path.exists(join(WORK_FOLDER, CACHE_FOLDER)):
    os.makedirs(join(WORK_FOLDER, CACHE_FOLDER))

if not os.path.exists(join(WORK_FOLDER, FIGURE_FOLDER)):
    os.makedirs(join(WORK_FOLDER, FIGURE_FOLDER))

seed=0
random.seed(seed)
np.random.seed(seed)

case_study_name = 'uci_adult'
data_folder = './datasets/uci_adult'
data_file = 'uci_adult_data.csv'
figure_folder = './figures'
emb_folder = './embeddings/'

df_data = data_loader.load(case_study_name, data_folder, data_file)

gender_col = pd.factorize(df_data['gender'])[0].tolist()
colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
          '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
          '#bcbd22', '#17becf']
marker_size = 20
ethnicity_col = pd.factorize(df_data['ethnicity'])[0].tolist()
markers = ['o', '^']
titles = ['(a)', '(b)', '(c)', '(d)']

income_col = pd.factorize(df_data["income"])[0].tolist()
'''
Compute embeddings with different prior
'''
emb_names = ['tsne', 'ctsne_prior_gender', 'ctsne_prior_ethnicity',
             'ctsne_prior_ethnicity_gender']
methods = ['tsne', 'ctsne', 'ctsne', 'ctsne']
prior_cols = [None, 'gender', 'ethnicity', 'ethnicity_gender']

embs = {}
for emb_name, prior_col, method in zip(emb_names, prior_cols, methods):
    emb_file = '{:s}_{:s}'.format(case_study_name, method)
    emb_file += '_{:s}'.format(prior_col) if prior_col else '.csv'
    emb_path = join(emb_folder, emb_file)
    embs[emb_name] = compute_embedding(df_data, emb_path, method=method,
                                       prior_col=prior_col)

def plot_embedding(ax, Y, gender_col, ethnicity_col, income_col, colors, markers,
                   marker_size, title):
    for c in np.unique(gender_col):
        for m in np.unique(ethnicity_col):
            for f in np.unique(income_col):
                mask = (gender_col == c) & (ethnicity_col == m) & (income_col == f)
                facecolors = colors[c] if f else 'none'
                edgecolors = 'none' if f else colors[c]
                marker_size = 12 if m == 1 else marker_size
                ax.scatter(Y[mask, 0], Y[mask, 1], marker_size, edgecolors=edgecolors,
                           marker=markers[m], facecolors=facecolors, alpha=0.8, linewidth=0.6)
    ax.annotate(title, xy=(0, 0.95), xycoords='axes fraction', fontsize=10)
    ax.set_axis_off()

figure_file = 'uci_adult.pdf'

fig, ax = plt.subplots(2, 2, figsize=(6, 6))
for i, emb_name in enumerate(emb_names):
    plot_embedding(ax[int(i/2)][i%2], embs[emb_name], gender_col, ethnicity_col,
                   income_col, colors, markers, 8, titles[i])
plt.subplots_adjust(left=0.00, bottom=0.00, right=1.0, top=0.98,
                    wspace=0.05, hspace=None)
fig.savefig(join(figure_folder, figure_file))
plt.close()
